# Xccelerate Engineering Offsite

## Getting Started

- `docker-compose up -d`
- `yarn`
- `yarn start:<PACKAGE_NAME>` OR `yarn start`

## Example

Sample Code

```js
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

readline.question(`What's your name?`, (name) => {
  console.log(`Hi ${name}!`)
  readline.close()
})
```

Submit code via cURL

```bash
curl --location --request POST 'http://localhost:3333/job' \
--header 'Content-Type: application/json' \
--data-raw '{
  "Request": {
    "Runtime": "Node",
    "Source": "\nconst readline = require('\''readline'\'').createInterface({\n  input: process.stdin,\n  output: process.stdout\n})\n\nreadline.question(`What'\''s your name?`, (name) => {\n  console.log(`Hi ${name}!`)\n  readline.close()\n})\n",
    "Input": [
      "Hello World!!"
    ]
  }
}'
```

Output

```json
{
  "Result": {
    "Runtime": "Node",
    "StartAt": "2020-07-31T02:38:00.793Z",
    "JobId": "51e5d1a8-81a8-4148-a619-d1e27780a21e"
  }
}
```

Query for job progress vis cURL

```bash
curl --location --request GET 'http://localhost:3333/job/51e5d1a8-81a8-4148-a619-d1e27780a21e'
```

Output

```text
retry: 3000

event: 51e5d1a8-81a8-4148-a619-d1e27780a21e
data: What's your name?

event: 51e5d1a8-81a8-4148-a619-d1e27780a21e
data: Hi Hello World!!!

```

## Design

### System

- Storage Backend provided by `Mongo` and `Redis`

- Communicate via [Bull Messaging](https://optimalbits.github.io/bull/) between Service, Worker

- Sandbox execution for submitted Code via [Child Process](https://nodejs.org/api/child_process.html) with Timeout at `10s`

- Streaming Interface for receiving Stdout without buffering whole output

### Depdendency

#### Model ([Ref](packages/model))

Storage Schemda provided by [Mongoose](https://mongoosejs.com/)

#### Task ([Ref](packages/task))

Generaal Messaging SDK wrapper around [Bull](https://optimalbits.github.io/bull/)

## Backend

[`yarn start:server`](packages/server)

Provided Service Backend with the following API

### **POST** `/job`

Request | `application/json`

```json
{
  "Request": {
    "Runtime": "Node",
    "Source": "STRING",
    "Input": [
      "STRING"
    ]
  }
}
```

Response | `application/json`

```json
{
  "Result": {
    "Runtime": "Node",
    "StartAt": "1970-01-01T00:00:00.000Z",
    "JobId": "UUID"
  }
}
```

### **GET** `/job/:jobId`

Response | `text/event-stream`

Content deliver via [Server-Sent Event](https://tinyurl.com/server-sent-event) for streaming result directly to Client Browser

```text
retry: 3000

event: STRING
data: STRING

```

## Backend Worker

[`yarn start:worker`](packages/worker)

Provided Background Daemon Worker for schdeuling Code Submission Task
