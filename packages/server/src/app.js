const app = require('express')()

const env = require('./env')
const error = require('./error')
const middleware = require('./middleware')
const schema = require('./schema')

const { Job } = require('@xccelerate-offsite/model')
const { Task } = require('@xccelerate-offsite/task')

const task = new Task(env.REDIS)

app.use(middleware.generateRquestId)

app.post('/job', middleware.bodyJson, middleware.checkSubmitJobRequest, async (req, res, next) => {
  const errors = schema.lastError(req)
  if (!errors.isEmpty()) {
    const err = new Error('error/invalid-request: ' + JSON.stringify(errors.array()))
    return error.withErr(400, err, res, next)
  }

  // Capture request from body
  const request = req.body

  // Construct Job record
  await Job.create({ JobId: req.reqId, ...request.Request })

  res.status(200).json({
    Result: {
      Runtime: request.Request.Runtime,
      StartAt: new Date(),
      JobId: req.reqId
    }
  })
})

app.get('/job/:jobId', middleware.checkQueryJobRequest, async (req, res, next) => {
  const errors = schema.lastError(req)
  if (!errors.isEmpty()) {
    const err = new Error('error/invalid-request: ' + JSON.stringify(errors.array()))
    return error.withErr(400, err, res, next)
  }

  // Capture request from params
  const request = req.params

  // Check that the Job Queried doest exists
  if (await Job.exists({ JobId: request.jobId }) === false) {
    const err = new Error('error/invalid-job-not-found')
    return error.withErr(404, err, res, next)
  }

  // Create reply channel
  const forReply = task.for(request.jobId)

  // And designate processing method via Server-Sent Event (https://tinyurl.com/server-sent-event)
  forReply.process((job, done) => {
    res.write(`event: ${request.jobId}\ndata: ${job.data.text}\n\n`)
    done()
  })

  // Setup Server-Sent Event Headers
  res.set({
    'Cache-Control': 'no-cache',
    'Content-Type': 'text/event-stream',
    'Connection': 'keep-alive'
  })

  // And flush all pending header
  res.flushHeaders()

  // Ask Client to retry in 3s if connection lost
  res.write('retry: 3000\n\n');

  try {
    // Submit task to Queue by "Runtime"
    const job = await task.forCodeSubmit.add('Node', {}, { jobId: request.jobId })
    // Wait for job to finish
    await job.finished()
  } catch (err) {
    res.write(`event: ${request.jobId}\ndata: ${err.message}\n\n`)
  }

  // Wait for output to drain
  await forReply.whenCurrentJobsFinished()

  // Close reply channel
  await forReply.close()

  res.end()
})

// Fall-through not handled route
app.use(error.notHandled)

// Error Logger
app.use(error.errorLogger)

// Error Catch-all
app.use(error.catchAll)

module.exports = app
