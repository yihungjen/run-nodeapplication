const env = require('./env')

function fmtErr({ err, defaultMessage = 'error/unexpected-error' } = {}) {
  const response = {
    error: {
      message: err !== undefined ? err.message : defaultMessage,
      stack: []
    }
  }
  if (env.IS_DEBUG === true) {
    response.error.stack = err !== undefined ? err.stack.split('\n') : []
  }
  return response
}

function withErr(code, err, res, next) {
  res.locals.statusCode = code
  res.locals.causeErr = err
  next(err)
}

async function notHandled(req, res, next) {
  res.locals.statusCode = 404
  res.locals.causeErr = new Error('Resource Not Found')
  next(res.locals.causeErr)
}

async function errorLogger(err, req, res, next) {
  console.error(req.reqId, new Date(), err)
  next(err)
}

async function catchAll(err, req, res, next) {
  if (res.headersSent) {
    return
  }
  const statusCode = res.locals.statusCode || 500
  const causeErr = res.locals.causeErr
  res.status(statusCode).json(fmtErr({ err: causeErr }))
}

module.exports = { withErr, notHandled, errorLogger, catchAll }
