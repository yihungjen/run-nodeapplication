const { checkSchema } = require('express-validator')

const bodyJson = require('body-parser').json({ type: 'application/json' })

async function generateRquestId(req, res, next) {
  const { v4: uuidv4 } = require('uuid')
  req.reqId = uuidv4()
  res.set('X-Request-By-Id', req.reqId)
  next()
}

const checkSubmitJobRequest = checkSchema({
  'Request.Runtime': {
    in: [ 'body' ],
    errorMessage: 'Invalid Required "Runtime" field',
    isIn: {
      options: [ 'Node' ]
    }
  },
  'Request.Source': {
    in: [ 'body' ],
    errorMessage: 'Invalid Required "Source" field',
    notEmpty: true,
    trim: true,
  },
  'Request.Input': {
    in: [ 'body' ],
    errorMessage: 'Invalid Required "Input" field',
    custom: {
      options: (value, { req }) => {
        return Array.isArray(req.body.Request.Input)
      }
    },
    customSanitizer: {
      options: (value) => {
        return value.map(item => item.endsWith('\n') ? item : item + '\n')
      }
    }
  }
})

const checkQueryJobRequest = checkSchema({
  'jobId': {
    in: [ 'params' ],
    errorMessage: 'Invalid Required "jobId" parameter',
    isUUID: true
  }
})

module.exports = { bodyJson, generateRquestId, checkSubmitJobRequest, checkQueryJobRequest }
