const mongoose = require('mongoose')

const Schema = mongoose.Schema

const JobSchema = Schema({
  JobId: { type: String, unique: true },
  Runtime: String,
  Source: Buffer,
  Input: [
    String
  ]
}, {
  timestamps: true
})

JobSchema.path('createdAt').index({ expires: '1m' })

const Job = mongoose.model('Job', JobSchema)

async function initConn(connectionUrl) {
  return await mongoose.connect(connectionUrl, {
    bufferCommands: false,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
  })
}

module.exports = {
  Job,
  initConn
}
