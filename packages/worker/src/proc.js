const { spawn } = require('child_process')
const { Readable } = require('stream')

async function spawnNode(filePath, inputArray, timeoutSec) {
  // Spwan and execute node script
  const proc = spawn('node', [filePath])

  // Write Stdin to Job Input Array
  Readable.from(inputArray).pipe(proc.stdin)

  // Create tombstone
  setTimeout(abortProc, timeoutSec * 1000, proc)

  return proc
}

function abortProc(proc) {
  if (proc.exitCode === null) {
    // Processing still running and not able to stop in due time
    proc.kill('SIGKILL')
  }
}

module.exports = { spawnNode }
